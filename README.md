This repository consists of three main directories: Dialogues, Lore and Storylines. Other files relating to the game's narrative will be in the main repository, including narrative design, level progression and other such design elements.

**Dialogues**: This directory will usually contain HTML files to be imported to Twine 2 at https://twinery.org/2/ in order to view and play the NPC dialogues in a text-based format.

**Lore**: This directory will usually contain plain text files describing any particular aspect of the game's lore. Once published here by a developer these are all canon elements of the world of Gasaron and can be used for reference when developing other content.

**Storylines**: This directory will contain plain text files consisting of a narrated summary of the main storyline and other stories that various quests may follow.

How to create storylines and storyline chapters:

The "main campaign" (the central plot followed by the game) is organised within its own folder under Storylines and divided by chapters. Each individual text file is one chapter of the story. Side-stories longer than one chapter should be similarily organised whithin a dedicated folder. Stories consisting of a single chapter should be submitted directly under Storylines.

To create a chapter specify the author, zone in which the story takes place and name/title of the story (for the main campaign it's simply "main campaign").

Example:
```
Author: Uruk-hai

Zone: Rohan

Story: Taking The Hobbits To Isengard, chapter 1
```

When the specific location of NPCs is relevant to the story, this can be specified using map coordinates (xx, yy; map hook). Quests tied to the story should be included using the following format:
```
QUEST: Name

Description, including goals, required items and any other relevant information.

Reward: should usually include experience and money along with any other desired reward.
```
